﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tinfo :MonoBehaviour {
	
	public GameObject terreno = null;
	public int index=-1;
    bool band = false;

    public Tinfo() { }
	public void Construir(List<GameObject> prefab_info,Vector3 P)
    {
        if(band == false)
        {       
			
			if (terreno == null) {
				terreno = Instantiate(prefab_info[index], P, prefab_info[index].transform.rotation) as GameObject;	

				if(terreno.GetComponent<InfoNodeQTMap> ()!=null)
					terreno.GetComponent<InfoNodeQTMap> ().index = index;
				
				band = true;					
			}
				

        }
        
    }
    public void Destruir()
    {
        if(band == true)
        {
            Destroy(terreno);
            band = false;      
			terreno = null;
        } 

    }
    
}
