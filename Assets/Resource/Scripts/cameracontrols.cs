﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameracontrols : MonoBehaviour {

	public Transform target;
	//public float speed;
    public float delta;
    Vector3 pof;
    Vector3 pof2;
    Vector3 dir;
    float distance;
    float deltaInterval;
    float interval = 0.0f;
    //Vector3 dir;
    public Vector3 psc;
    bool closeToCamera = true;
    bool closeToSphere = true;
	//bool isMoving = false;
    bool band = false;
    bool band2 = false;

    // Use this for initialization
    void Start()
    {
        pof = new Vector3(target.position.x + psc.x, target.position.y + psc.y, target.position.z + psc.z);
        this.transform.position = pof;
    } 
	// Update is called once per frame
	void Update () {
        /*	dir = target.transform.position.normalized;
            this.transform.Rotate (dir);*/
        /*if (!closeToCamera && !closeToSphere && !isMoving)
             Move();
         if (isMoving && this.transform.position == pof2)
             isMoving = false;*/
            
        if (band && interval < 1.0f && band2)
        {
            this.transform.localPosition += (dir * deltaInterval);
            //Debug.Log("2 band");
            interval += delta;
        }
        /*if ()
        {

        }*/
    }

    public void SetCTSphere(bool wannaMove2)
    {
        closeToSphere = wannaMove2;
    }

    void Move ()
    {
        //pof = new Vector3(target.position.x + psc.x, target.position.y + psc.y, target.position.z + psc.z);

        //this.transform.position = Vector3.Lerp(this.transform.position, pof2, speed * Time.deltaTime);
		//Debug.Log("Move");
        //isMoving = true;

    }
    /*public void SetMove(bool wannaMove)
    {
        isMoving = wannaMove;
    }*/
    /*void OnTriggerEnter(Collider other)
    {
        Debug.Log("TriggerEnter");
        closeToCamera = true;
    }*/
    void OnTriggerExit(Collider other)
    {
        Debug.Log("TriggerExit");
        //closeToCamera = false;
        /*if (isMoving && isMoving2)
        {
            pof2 = new Vector3(target.position.x + psc.x, target.position.y + psc.y, target.position.z + psc.z);
            isMoving2 = false;
        }*/
        if (!band)
        {
            band = (interval < 1.0f);
            Debug.Log("1 band");
        }
        SetPof2();
    }

    public void SetBand(bool bandera)
    {
        band = bandera;
        Debug.Log("Band: " + band);
    }

    public void SetPof2()
	{
		pof2 = new Vector3(target.position.x + psc.x, target.position.y + psc.y, target.position.z + psc.z);
        dir = (pof2 - this.transform.position).normalized;
        distance = Vector3.Distance(this.transform.position, pof2);
        deltaInterval = distance * delta;
        band2 = true;
        Debug.Log("SetPof2");
        interval = 0.0f;
    }
}
