﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateMap : MonoBehaviour {
    private Quadtree mEdQuadTree;
    public Transform max;
   	public GameObject point;
	public BoxCollider mbounds;
	public List<GameObject> prefab_info;
    public int stop;
	NodoQT m;
	// Use this for initialization
	void Start () {
        mEdQuadTree = new Quadtree(max.position, stop);
        mEdQuadTree.Splittree(stop);
		int cont = 0;
		mEdQuadTree.PreOrdenIndex (mEdQuadTree.root,ref cont);

    }
	void load_info_prefab()
	{


	}
	// Update is called once per frame
	void Update () {
		
        mEdQuadTree.Drawtree();
        //mEdQuadTree.Pertenece(point.transform.position);  

		Bounds b = mbounds.bounds;

        PreordenCreate(mEdQuadTree.root, b);


    }
    void PreordenCreate(NodoQT n,Bounds b) {


        if (n == null)
            return;
		
        if (n.Ishoja())
        {
            if (n.Perteneceboxcolaider(b))
            {
				n.info.Construir(prefab_info, n.min);
                n.DrawPertenece();

            }
            else
                n.info.Destruir();
        }

        PreordenCreate(n.hijo[0], b);
        PreordenCreate(n.hijo[1], b);
        PreordenCreate(n.hijo[2], b);
        PreordenCreate(n.hijo[3], b);


    }
	void OnDrawGizmosSelected() {

		if (m != null) {
			
			Gizmos.color = Color.blue;
			Gizmos.DrawLine(m.arrg[0], m.arrg[1]);
			Gizmos.DrawLine(m.arrg[1], m.arrg[2]);
			Gizmos.DrawLine(m.arrg[2], m.arrg[3]);
			Gizmos.DrawLine(m.arrg[3], m.arrg[0]);
		}



	}


}
