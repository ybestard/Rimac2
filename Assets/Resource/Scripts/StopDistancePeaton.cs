﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Peaton.Characters.ThirdPerson;
public class StopDistancePeaton : MonoBehaviour {
	public ThirdPersonCharacterPeaton mThirdPersonCharacterPeaton;
	float old_MoveSpeed=0;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Player") {
			old_MoveSpeed = mThirdPersonCharacterPeaton.m_MoveSpeed;
			mThirdPersonCharacterPeaton.m_MoveSpeed = 0;
		}

	}
	void OnTriggerStay(Collider other)
	{
		//mThirdPersonCharacterPeaton.m_MoveSpeed = 0;
		//Debug.Log (old_MoveSpeed);
	}
	void OnTriggerExit(Collider other)
	{
		//Debug.Log (other.gameObject.name);

		if (other.gameObject.tag == "Player") {
		mThirdPersonCharacterPeaton.m_MoveSpeed = old_MoveSpeed;
		old_MoveSpeed = 0;
		}
	}
}
