﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovedPlayer : MonoBehaviour {

    public SimpleTouchController leftController;   
    /// 
   
    public float speed;
	public float rotspeed;
	Vector3 movent;
	Vector3 rot;
	Rigidbody player;
	Animator AnimPlayer;
	float m_ForwardAmount;
	// Use this for initialization
	void Awake () {	
		player	= GetComponent<Rigidbody>();
		AnimPlayer = GetComponent<Animator> ();
	}

	// Update is called once per frame
	void FixedUpdate () {
        ///
        float h = leftController.GetTouchPosition.x;
        float v = leftController.GetTouchPosition.y;
        //float h = Input.GetAxisRaw("Horizontal");		
        //float v = Input.GetAxisRaw("Vertical");

        if (h != 0 || v != 0) 
			AnimPlayer.SetBool ("movent", true);		
		else
			AnimPlayer.SetBool ("movent", false);
		m_ForwardAmount = (new  Vector3 (h, 0, v)).magnitude;

		Rotation(h,v);
		Movimiento(h,v);
	}	

	void Movimiento (float h, float v)
	{
		
		movent.Set(h,0,v);		
		movent=movent*speed*Time.deltaTime;
		player.MovePosition (player.position + movent);

		UpdateAnimator(Vector3.zero);

	}
	void UpdateAnimator(Vector3 dir)
	{
		// update the animator parameters
		AnimPlayer.SetFloat("Forward", m_ForwardAmount, 0.1f, Time.deltaTime);
		AnimPlayer.speed = 1;

	}
	void Rotation(float h , float v){
		
		if(h!=0 || v!=0){

		if(v==1 && h==1)
			rot.Set (0f, 45, 0f);
		else if(v==-1 && h==-1)
			rot.Set (0f, 225, 0f);
		else if(v==1 && h==-1)
			rot.Set (0f, 315, 0f);
		else if(v==-1 && h==1)
			rot.Set (0f, 135, 0f);			
		else if (h >= 0.5f) 
			rot.Set (0f, 90f, 0f);		
			
		else if (h <= -0.5f)
			rot.Set (0f, 270, 0f);
			
		else if (v >= 0.5f) 
			rot.Set (0f, 0f, 0f);

		else if (v <= -0.5f)
			rot.Set (0f, 180, 0f);		
		
		Quaternion DeltaRotation = Quaternion.Euler (rot);
			//Debug.Log (DeltaRotation);
			player.MoveRotation (Quaternion.Lerp (player.rotation, DeltaRotation,rotspeed*Time.deltaTime));
		}
	}
}
