﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quadtree  {
    public NodoQT root { get; set; }
    int cont { get; set; }
    int Stop { get; set; }

    public Quadtree(Vector3 max, int s)
    {
        Stop = s;
        Vector3[] arrgv = new Vector3[4];
        arrgv[0] = new Vector3(-max.x, 0, max.z);
        arrgv[1] = new Vector3(max.x, 0, max.z);
        arrgv[2] = new Vector3(max.x, 0, -max.z);
        arrgv[3] = new Vector3(-max.x, 0, -max.z);

        root = new NodoQT(1, arrgv);
    }
    public void Splittree(int Stop)
    {
        
        SplitTreeAux(root, Stop, 1);

    }
    public void SplitTreeAux(NodoQT nodo, int stop, int cont)
    {
        if (cont == stop)
            return;
        nodo.split();
        for (int i = 0; i < nodo.hijo.Length; i++)
            SplitTreeAux(nodo.hijo[i], stop, cont + 1);
    }
 
    public void Drawtree()
    {
        DrawtreeAux(root);
    }

    private void DrawtreeAux(NodoQT nodo)
    {   
		if (nodo.Ishoja() == true)
        {
            nodo.Draw();

            return; }
        nodo.Draw();
        for (int i = 0; i < nodo.hijo.Length; i++)
            DrawtreeAux(nodo.hijo[i]);
        
    }
    public NodoQT Pertenece(Vector3 pos)
    {
        NodoQT aux = root;
        while (aux != null)
        {
            if (aux.Ishoja() && aux.PerteneceB(pos))
            {
                return aux;}

            else
            {
                bool flag = false;
                for (int i = 0; i < 4 && !flag; i++)
                {
                    if (aux.hijo[i].PerteneceB(pos))
                    {
                        aux = aux.hijo[i];
                        flag = true;
                    }
                    if (flag == false)
                        return null;
                }
            }
        }
        return null;
    }

    public void PreOrden(NodoQT n, int nivelpinta)
    {
        if (n == null)
            return;
        if (n.lv == nivelpinta)
           n.Draw();
            PreOrden(n.hijo[0], nivelpinta);
            PreOrden(n.hijo[1], nivelpinta);
            PreOrden(n.hijo[2], nivelpinta);
            PreOrden(n.hijo[3], nivelpinta);
        
    }


    public void InOrden(NodoQT n, int nivelpinta)
    {
        if (n == null)
            return;

        InOrden(n.hijo[0], nivelpinta);
        if (n.lv == nivelpinta)
            n.Draw();
        InOrden(n.hijo[1], nivelpinta);
        InOrden(n.hijo[2], nivelpinta);
        InOrden(n.hijo[3], nivelpinta);

    }

    public void PosOrden(NodoQT n, int nivelpinta)
    {
        if (n == null)
            return;
        PosOrden(n.hijo[0], nivelpinta);
        PosOrden(n.hijo[1], nivelpinta);
        PosOrden(n.hijo[2], nivelpinta);
        PosOrden(n.hijo[3], nivelpinta);
        if (n.lv == nivelpinta)
            n.Draw();
	}

	public void PreOrdenIndex(NodoQT nodo,ref int cont)
	{
		if (nodo.Ishoja() == true)
		{
			nodo.info.index = cont++;

			//Debug.Log ( "nodo.info.index  "+ nodo.info.index );
			return; }
		
		for (int i = 0; i < nodo.hijo.Length; i++)
			PreOrdenIndex(nodo.hijo[i],ref cont);
		

	}

}
