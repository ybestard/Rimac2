﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZonaSegura : MonoBehaviour {

    bool isInside = false;
    public GameObject player;
    public float waitTime;
    float enterTime;
    bool mensajeMandado = false;
	
	// Update is called once per frame
	void Update () {
        if (isInside && !mensajeMandado && Time.time >= (enterTime + waitTime))
        {
            player.GetComponent<BotarMensaje>().MostrarMensaje("Te resguardaste en una Zona Segura");
            mensajeMandado = true;
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            isInside = true;
            enterTime = Time.time;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
            isInside = false;
    }
}
