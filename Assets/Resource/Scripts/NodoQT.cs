﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodoQT  {
    public Tinfo info;
    public int lv;

    public NodoQT[] hijo = new NodoQT[4];
    public Vector3[] arrg = new Vector3[4];
    public Vector3 min, max;

	public NodoQT() { }
    public NodoQT(int l, Vector3[] arrgV)
    {
		
		lv = l;
        info = new Tinfo();
        hijo[0] = null;
        hijo[1] = null;
        hijo[2] = null;
        hijo[3] = null;

        arrg[0] = arrgV[0];
        arrg[1] = arrgV[1];
        arrg[2] = arrgV[2];
        arrg[3] = arrgV[3];

        min = arrg[3];
        max = arrg[1];
        for (int i = 1; i < 4; i++)
        {
            if (max.x <= arrg[i].x && max.z <= arrg[i].z)
                max = arrg[i];
        }
        for (int i = 1; i < 4; i++)
        {
            if (min.x >= arrg[i].x && min.y >= arrg[i].y && min.z >= arrg[i].z)
                min = arrg[i];
        }
    }	 
    public void split()
    {

        float h = lv * 10;
        //float REST = 16384;
		Vector3 V00 = new Vector3(arrg[0].x , h, arrg[0].z);
        Vector3 V01 = new Vector3((arrg[0].x + arrg[1].x) / 2, h, arrg[1].z);
		Vector3 V02 = new Vector3(V01.x, h,(arrg[0].z+ arrg[3].z)/2);
		Vector3 V03 = new Vector3(arrg[0].x, h, V02.z);
		Vector3 V11 = new Vector3(arrg[1].x, h, arrg[1].z);
		Vector3 V12 = new Vector3(arrg[1].x, h,V02.z);
		Vector3 V13 = new Vector3(arrg[2].x, h, arrg[2].z);
		Vector3 V21 = new Vector3(V02.x, h, arrg[2].z);
		Vector3 V31 = new Vector3(arrg[3].x, h, arrg[3].z);

        // Vector3 Rest(REST,0,REST);
        Vector3[] c1 = { V00, V01, V02, V03 };
        hijo[0] = new NodoQT(lv + 1, c1);
        //----------------------------------------------------------------------------------
        

        Vector3[] c2 = { V01, V11, V12, V02 };

        hijo[1] = new NodoQT(lv + 1, c2);
        //-------------------------------------------------------------------------------------
        
        Vector3[] c3 = { V02, V12, V13, V21 };


        hijo[2] = new NodoQT(lv + 1, c3);
        //-------------------------------------------------------------------------------------
        
        Vector3[] c4 = { V03, V02, V21, V31 };

        hijo[3] = new NodoQT(lv + 1, c4);
    }

    public bool PerteneceB(Vector3 p)
    {
        bool mMin = (p.x >= min.x && p.z >= min.z);
        bool mMax = (p.x <= max.x && p.z <= max.z);

        return (mMin && mMax);
    }

    public bool Ishoja()
    {

        return (hijo[0] == null);
    }
	public void Draw()
	{
		Debug.DrawLine(arrg[0], arrg[1], Color.green);
		Debug.DrawLine(arrg[1], arrg[2], Color.green);
		Debug.DrawLine(arrg[2], arrg[3], Color.green);
		Debug.DrawLine(arrg[3], arrg[0], Color.green);
	}

    public void DrawPertenece()
    {
        
		Debug.DrawLine(arrg[0], arrg[1], Color.red);
        Debug.DrawLine(arrg[1], arrg[2], Color.red);
        Debug.DrawLine(arrg[2], arrg[3], Color.red);
        Debug.DrawLine(arrg[3], arrg[0], Color.red);
		
    }

    public bool Perteneceboxcolaider(Bounds player)
    {
        Vector3 v3 = new Vector3(player.min.x, 0, player.min.z);
        Vector3 v1 = new Vector3(player.max.x, 0, player.max.z);
        Vector3 v0 = new Vector3(v3.x, 0, v1.z);
        Vector3 v2 = new Vector3(v1.x, 0, v3.z);

        bool V3= PerteneceB(v3);
        bool V1= PerteneceB(v1);
        bool V0 = PerteneceB(v0);
        bool V2  = PerteneceB(v2);
        return (V3 || V1 || V2 || V0);        
    }



}
