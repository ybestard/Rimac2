﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereScript : MonoBehaviour {

    public GameObject Camera;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerExit(Collider other)
    {
        Debug.Log("ExitSphere");
        Camera.GetComponent<cameracontrols>().SetCTSphere(false);
		//Camera.GetComponent<cameracontrols>().SetPof2();

    }
	void OnTriggerEnter(Collider other)
    {
        Debug.Log("EnterSphere");
        Camera.GetComponent<cameracontrols>().SetCTSphere(true);
        Camera.GetComponent<cameracontrols>().SetBand(true);

    }
}
