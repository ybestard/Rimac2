﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class transparence : MonoBehaviour {
    Renderer rend;
    Color color = new Color(1, 1, 1, 1f);
    bool band = false;
  //  public GameObject objcolition;


    // Use this for initialization
    void Start() {
        rend = GetComponent<Renderer>();
       //0 rend.material.shader = Shader.Find("Standard-2Sided");
     }

    // Update is called once per frame
    void Update()
    {

        if (band==true)
        {
            color.a = Mathf.Lerp(color.a, color.a + 0.05f, Time.deltaTime * 40);
            color.a = Mathf.Clamp(color.a, 0.1f, 1.0f);

            rend.material.SetColor("_Color", color);
            if (color.a <= 0.2f)
                band = false;
        }
       /* else
        {
            color.a = Mathf.Lerp(color.a, color.a + 0.05f, Time.deltaTime * 40);
            color.a = Mathf.Clamp(color.a, 0.1f, 1.0f);

            rend.material.SetColor("_Color", color);
            //if (color.a >= 1)
            //    band = false;
        }*/
    }

    void OnTriggerEnter(Collider other)
    {
        band = false;
    }
    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "CameraPlayer")
        {
            band = false;

            color.a = Mathf.Lerp(color.a, color.a - 0.05f, Time.deltaTime * 40);
            color.a = Mathf.Clamp(color.a, 0.2f, 1.0f);
            
            rend.material.SetColor("_Color", color);
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "CameraPlayer")
        {
            band = true;
        }
    }
}
