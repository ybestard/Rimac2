﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Peaton.Characters.ThirdPerson;
public class PeatonScript : MonoBehaviour {

	public enum MoveState
	{
		stay,
		walk,
		run
	}
		
	private Vector3 direction;
	private float speed;
	private MoveState moveState;

	[System.NonSerialized]
	public GameObject preNode;
	[System.NonSerialized]
	public GameObject targetNode;


	private const float THINKS_TIME = 1.0f;
	private float timeToThinks;


	// Use this for initialization
	void Start () {

		this.timeToThinks = 0.0f;
		this.moveState = MoveState.stay;
		this.direction = new Vector3 (0.0f, 0.0f, 1.0f);
		this.speed = 0.1f;
		if(this.targetNode!=null && GetComponent<AICharacterControlPeaton> ()!=null)
		GetComponent<AICharacterControlPeaton> ().target = this.targetNode.transform;
	}
	
	// Update is called once per frame
	void Update () {

		desplazar ();

	}

	public void desplazar(){

		switch (this.moveState) {

		case MoveState.stay:
			{
				timeToThinks += Time.deltaTime;

				if(timeToThinks >= THINKS_TIME){
					if (preNode != null) {
						this.targetNode = this.preNode.GetComponent<NodeScript> ().getRandomNode ();
						direction = this.targetNode.transform.position - this.transform.position;
						this.moveState = MoveState.walk;
						timeToThinks = 0.0f;	
					}
				}


			}
			break;

		case MoveState.walk:
			{
				if (this.targetNode != null && GetComponent<AICharacterControlPeaton> () != null)
					GetComponent<AICharacterControlPeaton> ().target = this.targetNode.transform;
				else {
					this.direction.Normalize ();

					this.transform.LookAt (this.transform.position + direction);
					this.transform.position += direction * speed;
				}



			}
			break;


		}

	}

	public void pensar(){


	}

	void OnTriggerEnter(Collider node){


		if (node.gameObject.tag == "node" && (this.targetNode == null || node.gameObject.GetInstanceID () == this.targetNode.GetInstanceID ())) {

			this.moveState = MoveState.stay;
			this.preNode = node.gameObject;
			//Debug.Log(preNode);
		}

	}
}
