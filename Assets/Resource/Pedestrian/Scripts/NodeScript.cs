﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeScript : MonoBehaviour {

	public List<GameObject> nodes;
	public GameObject prefab;
	// Use this for initialization
	void Start () {
		Instantiate(prefab, transform.position, transform.rotation) ;	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void AddNode(GameObject n){

		nodes.Add (n);


	}

	void OnDrawGizmos(){

		foreach (GameObject n in nodes) {

			Gizmos.color = Color.red;
			Gizmos.DrawLine (this.transform.position, n.transform.position);

		}

	}

	public GameObject getRandomNode(){

		return this.nodes[Random.Range (0,this.nodes.Count)];
	}
}
