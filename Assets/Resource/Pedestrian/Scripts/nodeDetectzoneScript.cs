﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class nodeDetectzoneScript : MonoBehaviour {

	public GameObject myNode;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider node){

		if (node.tag == "node" && node.gameObject.GetInstanceID () != myNode.GetInstanceID ()) {

			myNode.GetComponent<NodeScript> ().AddNode(node.gameObject);

		}

	}

}
