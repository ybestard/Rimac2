using UnityEngine;

namespace Peaton.Characters.ThirdPerson
{
	//[RequireComponent(typeof(Rigidbody))]
	[RequireComponent(typeof(CapsuleCollider))]
	[RequireComponent(typeof(Animator))]
	public class ThirdPersonCharacterPeaton : MonoBehaviour
	{
		[SerializeField] float m_MovingTurnSpeed = 360;
		/*[SerializeField] float m_StationaryTurnSpeed = 180;
		[SerializeField] float m_JumpPower = 12f;
		[Range(1f, 4f)][SerializeField] float m_GravityMultiplier = 2f;
		[SerializeField] float m_RunCycleLegOffset = 0.2f; //specific to the character in sample assets, will need to be modified to work with others*/
		[SerializeField] float m_MoveSpeedMin = 1f;
		[SerializeField] public float m_MoveSpeed = 5f;
		[SerializeField] float m_MoveSpeedMax = 10f;
		/*[SerializeField] float m_AnimSpeedMultiplier = 1f;
		[SerializeField] float StopDistance = 0.1f;*/

		//Rigidbody m_Rigidbody;
		Animator m_Animator;
		/*bool m_IsGrounded;
		float m_OrigGroundCheckDistance;
		const float k_Half = 0.5f;
		float m_TurnAmount;*/
		float m_ForwardAmount;
		/*Vector3 m_GroundNormal;
		float m_CapsuleHeight;
		Vector3 m_CapsuleCenter;
		CapsuleCollider m_Capsule;
		bool m_Crouching;*/


		void Start()
		{
			m_Animator = GetComponent<Animator>();
			//m_Rigidbody = GetComponent<Rigidbody>();
			/*m_Capsule = GetComponent<CapsuleCollider>();
			m_CapsuleHeight = m_Capsule.height;
			m_CapsuleCenter = m_Capsule.center;*/

			//m_Rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
		}


		public void Move(Vector3 pos, bool crouch, bool jump)
		{
			
			Vector3 dir = pos -transform.position;
			//StopDistanceObject ();
			dir.Normalize ();
			m_ForwardAmount = Mathf.Clamp01(m_MoveSpeed);

			transform.position = Vector3.Lerp(transform.position ,transform.position + dir*m_MoveSpeed,Time.deltaTime);
				



			ApplyExtraTurnRotation(pos);


			// send input and other state parameters to the animator
			UpdateAnimator(dir);
		}



		void UpdateAnimator(Vector3 dir)
		{
			// update the animator parameters
			m_Animator.SetFloat("Forward", m_ForwardAmount, 0.1f, Time.deltaTime);
			m_Animator.speed = 1;

		}




		void ApplyExtraTurnRotation(Vector3 target)
		{

			/*Quaternion targetRotation = Quaternion.Slerp (transform.rotation ,Quaternion.LookRotation (target - transform.position),Time.deltaTime*  m_MovingTurnSpeed);	
			targetRotation.x = 0;
			targetRotation.z = 0;*/

			//transform.rotation = Quaternion.Slerp (transform.rotation, targetRotation,Time.deltaTime *  m_MovingTurnSpeed);
			transform.LookAt(target);
		}



	}
}
