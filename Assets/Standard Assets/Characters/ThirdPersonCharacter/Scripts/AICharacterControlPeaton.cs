using System;
using UnityEngine;

namespace Peaton.Characters.ThirdPerson
{
    
	[RequireComponent(typeof (ThirdPersonCharacterPeaton))]
    public class AICharacterControlPeaton : MonoBehaviour
    {
        
		public ThirdPersonCharacterPeaton character { get; private set; } // the character we are controlling
        public Transform target;                                    // target to aim for


        private void Start()
        {
            // get the components on the object we need ( should not be null due to require component so no need to check )
            
			character = GetComponent<ThirdPersonCharacterPeaton>();

        }


        private void Update()
        {
			if(target!=null)
			character.Move (target.transform.position,false,false);

            /*if (target != null)
                agent.SetDestination(target.position);

            if (agent.remainingDistance > agent.stoppingDistance)
                character.Move(agent.desiredVelocity, false, false);
            else
                character.Move(Vector3.zero, false, false);*/
        }


        public void SetTarget(Transform target)
        {
            this.target = target;
        }
    }
}
